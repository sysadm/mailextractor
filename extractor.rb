#!/usr/bin/env ruby -W0

require 'mail'
require 'pry'

class Extractor
  RESTRICTED = true

  def extract_emails(str)
    str.scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i).uniq
  rescue
    []
  end

  def run
    address_array = []
    Dir.glob('emls/*.eml') do |eml|
      file = File.read(eml)
      mail = Mail.read_from_string file
      decoded_body = RESTRICTED ? mail.body.decoded.delete("=\n") : mail.body.decoded
      result = [mail.to, mail.cc, mail.from, decoded_body].flatten.compact.uniq
        .map{|str| extract_emails str}.flatten.compact.uniq
      address_array << result
      print "File: #{eml}, unique email count: #{result.count}\n"
    end
    file = File.open("addr_list.csv", "w+b", 0644)
    file.write (address_array.flatten.uniq.join(",\n") + ",\n")
    file.close
    print "Sumaric unique extracted emails from all files: #{address_array.flatten.uniq.count}\n"
  end
end

extractor = Extractor.new
extractor.run
